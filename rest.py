from flask import Flask, jsonify
from flask_cors import CORS, cross_origin
import pandas as pd
import pandas_datareader as pdr
from pandas_datareader import data
import datetime


app = Flask(__name__)
cors = CORS(app)

#return live price data for the stock
@app.route('/<string:ticker>/', methods=['GET'])
def getPrice(ticker):
    return jsonify(data.DataReader(ticker, data_source='yahoo')['Adj Close'][-1])

#return live price data in a particular price type
@app.route('/<string:ticker>/<string:price_type>', methods=['GET'])
def getCustomPriceType(ticker, price_type):
    return jsonify(data.DataReader(ticker, data_source='yahoo')[price_type][-1])

#return live price data in a particular price type and time
@app.route('/<string:ticker>/<string:price_type>/<string:date>', methods=['GET'])
def GetPriceOnDate(ticker, price_type, date):
    return jsonify(data.DataReader(ticker,data_source='yahoo').loc[date, [price_type]].tolist())

#return live price data of a particular price type in a range of dates
@app.route('/<string:ticker>/<string:price_type>/<string:start_date>/<string:end_date>', methods=['GET'])
def getPriceRange(ticker, price_type, start_date, end_date):
    return jsonify(data.DataReader(ticker,
                        start=start_date,
                        end=end_date,  
                       data_source='yahoo')[price_type].tolist())

#bollinger Band calculation
def bollingerBand(ticker):
    
        start_date = datetime.datetime.now() - datetime.timedelta(40)
        end_date=datetime.datetime.now()

        df = pdr.get_data_yahoo(ticker, start_date, end_date)[['Close']]
        df['mavg'] = df['Close'].rolling(window=21).mean()
        df['std'] = df['Close'].rolling(window=21).std()
        df['Upper Band'] = df['mavg'] + (df['std'] * 2)
        df['Lower Band'] = df['mavg'] - (df['std'] * 2)
        currentPrice = df.iloc[-1]['Close']
        currentUB = df.iloc[-1]['Upper Band'].round(2)
        currentLB = df.iloc[-1]['Lower Band'].round(2)
        advice = ""
        if currentPrice >= currentUB:
            advice = "Sell"
        elif currentPrice <= currentLB:
            advice = "Buy"
        else:
            advice = "Hold"


        print(str(df))

        df['Date'] = df.index

        dict = {'ticker': ticker,
                'date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
                'price': df.iloc[-1]['Close'],
                'Upper Band': currentUB,
                'Lower Band': currentLB,
                'Advice': advice}
        return dict

# data for Bollinger Band plotting
def bollingerBandPlot(ticker):
        start_date = datetime.datetime.now() - datetime.timedelta(40)
        end_date=datetime.datetime.now()

        df = pdr.get_data_yahoo(ticker, start_date, end_date)[['Close']]
        df['mavg'] = df['Close'].rolling(window=21).mean()
        df['std'] = df['Close'].rolling(window=21).std()
        df['Upper Band'] = df['mavg'] + (df['std'] * 2)
        df['Lower Band'] = df['mavg'] - (df['std'] * 2)

        return df
   
#return the advice for the stock
@app.route('/advice/<string:ticker>', methods=['GET'])
def getAdvice(ticker):
        dict = bollingerBand(ticker)
        return jsonify(dict['Advice'])

#return the latest Upper Band value
@app.route('/advice/upperband/<string:ticker>', methods=['GET'])
def getUpperband(ticker):
        dict = bollingerBand(ticker)
        return jsonify(dict['Upper Band'])

#return the latest Lower Band value
@app.route('/advice/lowerband/<string:ticker>', methods=['GET'])
def getLowerband(ticker):
        dict = bollingerBand(ticker)
        return jsonify(dict['Lower Band'])




#return the list of Close, Upper Band, Lower Band and mavg for Bollinger Band plot
@app.route('/moreinformation/<string:ticker>', methods=['GET'])
def getAll(ticker):
        df=bollingerBandPlot(ticker)
        df = df.dropna(axis=0,how='any')

        return jsonify([df['Close'].tolist(),df['Upper Band'].tolist(),df['Lower Band'].tolist(),df['mavg'].tolist()])




if __name__ == '__main__':
    app.run(host='0.0.0.0')
